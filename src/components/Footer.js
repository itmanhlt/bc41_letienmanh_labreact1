import React, { Component } from "react";

export default class Footer extends Component {
  render() {
    return (
      <div id="footer" className="py-5">
        <p>Copyright © Your Website 2022</p>
      </div>
    );
  }
}
