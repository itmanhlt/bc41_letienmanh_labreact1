import React, { Component } from "react";
import "../sass/Main.scss";

export default class Banner extends Component {
  render() {
    return (
      <div className="banner p-5">
        <div className="banner-content py-5 bg-light">
          <div className="banner-content-child py-5 px-5">
            <div className="px-5">
              <h1 className="font-weight-bold">A warm welcome!</h1>
              <p>
                Bootstrap utility classes are used to create this jumbotron
                since the old component has been removed from the framework. Why
                create custom CSS when you can use utilities?
              </p>
              <button className="btn btn-primary btn-lg">Call to action</button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
