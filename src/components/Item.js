import React, { Component } from "react";
import "../sass/Main.scss";

export default class Item extends Component {
  data = [
    {
      class: "bi bi-collection",
      title: "Fresh new layout",
      content:
        "With Bootstrap 5, we've created a fresh new layout for this template",
    },
    {
      class: "bi bi-cloud-download",
      title: "Free to download",
      content:
        "As always, Start Bootstrap has a powerful collectin of free templates.",
    },
    {
      class: "bi bi-card-heading",
      title: "Jumbotron hero header",
      content: "The heroic part of this template is the jumbotron hero header!",
    },
    {
      class: "bi bi-bootstrap",
      title: "Feature boxes",
      content: "We've created some custom feature boxes using Bootstrap icons!",
    },
    {
      class: "bi bi-code",
      title: "Simple clean code",
      content:
        "We keep our dependencies up to date and squash bugs as they come!",
    },
    {
      class: "bi bi-patch-check",
      title: "A name you trust",
      content:
        "Start Bootstrap has been the leader in free Bootstrap templates since 2013!",
    },
  ];
  renderList = () => {
    return this.data.map((item) => {
      return (
        <div className="item-content bg-light">
          <div className="item-content-card bg-light">
            <div className="card m-5 bg-light">
              <div className="card-top">
                <i className={item.class}></i>
              </div>
              <div className="card-body">
                <h2>{item.title}</h2>
                <p className="card-text">{item.content}</p>
              </div>
            </div>
          </div>
        </div>
      );
    });
  };
  render() {
    return <div className="item">{this.renderList()}</div>;
  }
}
