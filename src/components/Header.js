import React, { Component } from "react";

import "../sass/Main.scss";

export default class Header extends Component {
  render() {
    return (
      <div>
        <header>
          <div id="header">
            <nav className="navbar navbar-expand-lg navbar-dark px-lg-5">
              <a className="navbar-brand" href="#">
                Start Bootstrap
              </a>
              <div
                className="collapse navbar-collapse justify-content-end"
                id="navbarNav"
              >
                <ul className="navbar-nav">
                  <li className="nav-item active">
                    <a className="nav-link" href="#">
                      Home <span className="sr-only">(current)</span>
                    </a>
                  </li>
                  <li className="nav-item">
                    <a className="nav-link" href="#">
                      About
                    </a>
                  </li>
                  <li className="nav-item">
                    <a className="nav-link" href="#">
                      Contact
                    </a>
                  </li>
                </ul>
              </div>
            </nav>
          </div>
        </header>
      </div>
    );
  }
}
